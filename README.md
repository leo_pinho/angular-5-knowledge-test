# AlanaTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

# Content

This project contains 2 types of exercises:

1- Given a number "n", get the sum of the squared dividers of it "f(n)". Given an interval of numbers,
get all f(n)'s of them and check wich ones are a perfect square, it shows the result in an Array like the example below:

[{number: 42, value: 2500}] for the interval [10,90].

2- Create a promise to manage an Http Post Request, the exercise include the management of the request, form validations and
UI notifications.
