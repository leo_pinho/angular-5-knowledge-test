import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import {Post} from '../models/Post';

@Injectable()
export class TestService {
  apiRoot = 'https://jsonplaceholder.typicode.com/posts';
  results: object;
  loading: boolean;
  constructor(private http: HttpClient) {
    this.loading = false;
  }
  getResponse(post: Post): Promise <any> {
    const promise = new Promise((resolve, reject) => {
      this.http.post(this.apiRoot, post)
        .toPromise()
        .then(
          res => { // Success
            this.results = res;
            resolve();
          },
          msg => { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }
}
