import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstExerciseComponent } from './templates/first-exercise/first-exercise.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SecondExerciseComponent } from './templates/second-exercise/second-exercise.component';
import { TestService } from './shared/services/TestService';
import { HomeComponent } from './templates/home/home.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'firstExercise', component: FirstExerciseComponent},
  {path: 'secondExercise', component: SecondExerciseComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FirstExerciseComponent,
    SecondExerciseComponent,
    HomeComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [ TestService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
