import { Component, OnInit } from '@angular/core';
import {TestService} from '../../shared/services/TestService';
import { Post } from '../../shared/models/Post';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-second-exercise',
  templateUrl: './second-exercise.component.html',
  styleUrls: ['./second-exercise.component.css']
})
export class SecondExerciseComponent implements OnInit {
  loading: boolean;
  postTitle: string;
  constructor(public testService: TestService) {
    this.loading = false;
  }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    this.loading = true;
    this.testService.getResponse(new Post(Math.random(), Math.random(), form.value.postTitle))
      .then( () => {
        this.loading = false;
      }).catch(error => {
        console.log(error);
    });
  }
}
