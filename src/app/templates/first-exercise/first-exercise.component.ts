import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-first-exercise',
  templateUrl: './first-exercise.component.html',
  styleUrls: ['./first-exercise.component.css']
})
export class FirstExerciseComponent implements OnInit {

  firstValue: number;
  secondValue: number;
  result: Array < any > = [];
  errorNumber: boolean;
  constructor() {
   }

  ngOnInit(): void {

  }
  onSubmit(form: NgForm) {
    this.result = [];
    this.firstValue = form.value.firstValue;
    this.secondValue = form.value.secondValue;
    for (let value = this.firstValue; value <= this.secondValue; value++) {
      const dividers  = this.getDividers(value);
      if (dividers && dividers.length) {
        const squaresSum = this.getSquaresSum(dividers);
        if (this.checkPerfectSquare(squaresSum)) {
          const object = {number: value, value: squaresSum };
          this.result.push(object);
        }
      }
    }
    console.log((this.result));
  }
  getDividers(value: number) {
    const dividers = [];
    let i: number;
    let division: number;
    let result: boolean;
    if (value) {
      for (i = 1; i <= value; i++) {
        division = value / i;
        result = (division - Math.floor(division)) !== 0;
        if (!result) {
          dividers.push(division);
        }
      }
    }
  return dividers;
  }
  getSquaresSum(numbers: any[]) {
    let sum = 0;
    for (const value of numbers) {
      sum = Math.pow(value, 2) + sum;
    }
    return sum;
  }
  checkPerfectSquare(value: number) {
    const result = Math.sqrt(value);
    const perfectSquare = (result - Math.floor(result)) === 0;
    return perfectSquare;
  }
  /** A hero's name can't match the given regular expression */

  checkValues() {
    this.errorNumber = false;
    if (this.firstValue > this.secondValue) {
      this.errorNumber = true;
    }
    return this.errorNumber;
  }
}
